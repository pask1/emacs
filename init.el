;; init.el

;;; Init and package configuration

(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
;; Load custom plugins
(add-to-list 'load-path "~/.config/emacs/my_plugins/")
;; Init use-package on non-linux plats
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

;;; General configuration

(use-package emacs
  :preface
  (defvar pask/indent-width 4)
  :config
  (setq frame-title-format '("emacs")
	ring-bell-function 'ignore
	frame-resize-pixelwise t
	default-directory "~/")

    ;;;###autoload
  (defun +embark:find-file-h (target) (split-window-below) (other-window 1) (find-file target))

  ;;;###autoload
  (defun +embark:split-vertical-current-completion-candidate () (interactive) (embark--act #'find-file-other-window (car (embark--targets)) t))

  ;;;###autoload
  (defun +embark:split-horizontal-current-completion-candidate () (interactive) (embark--act #'+embark:find-file-h (car (embark--targets)) t))
  
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (tooltip-mode -1)
  (scroll-bar-mode -1)
  ;; (set-fringe-mode 0)
  (column-number-mode 1)
  (global-display-line-numbers-mode 1)
  (setq display-line-numbers-type 'relative)

  ;; set transparency
  (set-frame-parameter nil 'alpha-background 75)
  (add-to-list 'default-frame-alist '(alpha-background . 75))

  (defun kb/toggle-window-transparency ()
    "Toggle transparency."
    (interactive)
    (let ((alpha-transparency 75))
      (pcase (frame-parameter nil 'alpha-background)
        (alpha-transparency (set-frame-parameter nil 'alpha-background 100))
        (t (set-frame-parameter nil 'alpha-background alpha-transparency)))))


  (setf (cdr (assq 'continuation fringe-indicator-alist))
      ;; '(nil nil) ;; no continuation indicators
       '(nil right-curly-arrow) ;; right indicator only
      ;; '(left-curly-arrow nil) ;; left indicator only
      ;; '(left-curly-arrow right-curly-arrow) ;; default
      )

  ;; Set font
  (set-face-attribute 'default nil :font "Consolas NF" :height 110)
  (add-to-list 'default-frame-alist '(font . "Consolas NF-11"))

  ;; Make ESC quit prompts
  (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

  ;; Make ESC stop closing windows
  (define-key key-translation-map (kbd "ESC") (kbd "C-g"))

  ;; Better scrolling experience
  (setq scroll-margin 0
    scroll-conservatively 101 ; > 100
    scroll-preserve-screen-position t
    auto-window-vscroll nil)

  ;; Always use spaces for indentation
  (setq-default indent-tabs-mode nil
    tab-width pask/indent-width)

  ;; Pls no startup msg
  (setq inhibit-startup-message t)

  (setq custom-file "~/.config/emacs/custom.el")
  (load custom-file))

;;; Third-party packages

;; Functionality and completion

(use-package vertico
  :ensure t
  :bind (("C-x C-f" . find-file)
         :map vertico-map
              ("C-j" . vertico-next)
              ("C-k" . vertico-previous)
              ("C-v" . #'+embark:split-vertical-current-completion-candidate)
              ("C-x" . #'+embark:split-horizontal-current-completion-candidate))
  :custom
  (vertico-cycle t)
  :init
  (vertico-mode)
  (vertico-multiform-mode)

  ; Config to set up highlighting in the completion buffer

  (defvar +vertico-transform-functions nil)
  
  (cl-defmethod vertico--format-candidate :around
    (cand prefix suffix index start &context ((not +vertico-transform-functions) null))
    (dolist (fun (ensure-list +vertico-transform-functions))
      (setq cand (funcall fun cand)))
    (cl-call-next-method cand prefix suffix index start))
  
  (defun +vertico-highlight-directory (file)
    "If FILE ends with a slash, highlight it as a directory."
    (if (string-suffix-p "/" file)
        (propertize file 'face 'marginalia-file-priv-dir) ; or face 'dired-directory
      file))
  
  ;; add-to-list works if 'file isn't already in the alist
  ;; setq can be used but will overwrite all existing values
  (add-to-list 'vertico-multiform-categories
               '(file
                 ;; this is also defined in the wiki, uncomment if used
                 ;; (vertico-sort-function . sort-directories-first)
                 (+vertico-transform-functions . +vertico-highlight-directory))))

(use-package consult
  :ensure t
  :bind (("M-x" . execute-extended-command)
         ("C-x b" . consult-buffer)
         ("C-s" . consult-line)))

(use-package embark
  :ensure t

  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  ;; Show the Embark target at point via Eldoc.  You may adjust the Eldoc
  ;; strategy, if you want to see the documentation from multiple providers.
  (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package corfu
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  (corfu-auto-prefix 2)
  (corfu-auto-delay 0.0)
  (corfu-quit-at-boundary 'separator)
  (corfu-echo-documentation 0.25)
  (corfu-preview-current 'insert)
  (corfu-preselect-first nil)
  (corfu-preselect 'prompt)
  :bind (:map corfu-map
              ("M-SPC"      . corfu-insert-separator)
              ("RET"        . nil)
              ("TAB"        . corfu-next)
              ([tab]        . corfu-next)
              ("S-TAB"      . corfu-previous)
              ([backtab]    . corfu-previous)
              ("S-<return>" . corfu-insert))
  :init
  (global-corfu-mode)
  (corfu-history-mode))

(use-package cape
  :defer 10
  :init
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-yasnippet)
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify)
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)

  (defun my/eglot-capf ()
  (setq-local completion-at-point-functions
              (list (cape-super-capf
                     #'eglot-completion-at-point
                     #'cape-file
                     #'cape-yasnippet))))

  (add-hook 'eglot-managed-mode-hook #'my/eglot-capf))

(add-to-list 'exec-path "/home/nils/.yarn/bin")

(use-package eglot
  :ensure t
  :config
  (add-to-list 'eglot-server-programs
               '((c++-mode c-mode) "clangd")
               '((typescript-mode) "typescript-language-server" "--stdio"))

  (add-hook 'c-mode-hook 'eglot-ensure)
  (add-hook 'c++-mode-hook 'eglot-ensure)
  (add-hook 'typescript-mode-hook 'eglot-ensure)

  (cl-defmethod project-root ((project (head eglot-project)))
  (cdr project))
  
  (defun my-project-try-tsconfig-json (dir)
    (when-let* ((found (locate-dominating-file dir "tsconfig.json")))
      (cons 'eglot-project found)))

  (add-hook 'project-find-functions
            'my-project-try-tsconfig-json nil nil))

(use-package flymake-diagnostic-at-point
  :after eglot flymake
  :config
  (add-hook 'flymake-mode-hook #'flymake-diagnostic-at-point-mode))


(use-package yasnippet
  :diminish yas-minor-mode
  :hook (prog-mode . yas-minor-mode)
  :config
  (yas-reload-all))

(use-package yasnippet-snippets
  :ensure t
  :after yasnippet)

(require 'cape-yasnippet)

; Might get rid of this later
(use-package consult-yasnippet
  :ensure t)

(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :init
  (smartparens-mode)
  :config

  ;; Smartparens auto-indent
  ;; Source: https://xenodium.com/emacs-smartparens-auto-indent/
  (defun indent-between-pair (&rest _ignored)
    (newline)
    (indent-according-to-mode)
    (forward-line -1)
    (indent-according-to-mode))

  (sp-local-pair 'prog-mode "{" nil :post-handlers '((indent-between-pair "RET")))
  (sp-local-pair 'prog-mode "[" nil :post-handlers '((indent-between-pair "RET")))
  (sp-local-pair 'prog-mode "(" nil :post-handlers '((indent-between-pair "RET"))))

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(use-package marginalia
  :after vertico
  :ensure t
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(use-package savehist
  :init
  (savehist-mode))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-undo-system 'undo-redo)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'emacs)
  (evil-set-initial-state 'eshell-mode 'emacs))

  (add-hook 'eshell-mode (lambda () (local-set-key (kbd "C-w") 'evil-window-map)))

(use-package evil-commentary
  :config
  (evil-commentary-mode))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :bind
  ([remap describe-function] . helpful-callable)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key))

;; UI elements and styling

(use-package dashboard
  :config
  (setq show-week-agenda-p t)
  (setq dashboard-items '((recents . 15) (agenda . 5)))
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-set-init-info t)
  (setq dashboard-startup-banner 3)
  (setq dashboard-set-navigator t)
  (setq dashboard-banner-logo-title "Welcome")
  (setq dashboard-startup-banner "~/.config/emacs/dash-logo-2.txt")
  (setq dashboard-center-content t)

  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))

  (setq dashboard-items '((recents  . 5)
                          (bookmarks . 5)
                          (registers . 5)))

  (dashboard-setup-startup-hook))

(add-to-list 'custom-theme-load-path "~/.config/emacs/themes/")
(add-to-list 'custom-theme-load-path "~/.config/emacs/themes/everforest/")
(load-theme 'everforest-hard-dark t)

(use-package all-the-icons
  :if (display-graphic-p))

(use-package centaur-tabs
  :demand
  :config
  (setq centaur-tabs-show-navigation-buttons t
        centaur-tabs-height 32
        centaur-tabs-set-icons t
        centaur-tabs-set-bar 'under
        x-underline-at-descent-line t
        centaur-tabs-left-edge-margin nil)
  (centaur-tabs-change-fonts (face-attribute 'default :font) 110)
  ;; (centaur-tabs-mode t)

  ; Config to set up centaur tab groups correctly

  (defun centaur-tabs-buffer-groups ()
    "`centaur-tabs-buffer-groups' control buffers' group rules.
  
    Group centaur-tabs with mode if buffer is derived from `eshell-mode' `emacs-lisp-mode' `dired-mode' `org-mode' `magit-mode'.
    All buffer name start with * will group to \"Emacs\".
    Other buffer group by `centaur-tabs-get-group-name' with project name."
    (list
     (cond
      ;; ((not (eq (file-remote-p (buffer-file-name)) nil))
      ;; "Remote")
      ((or (string-equal "*" (substring (buffer-name) 0 1))
           (memq major-mode '(magit-process-mode
                              magit-status-mode
                              magit-diff-mode
                              magit-log-mode
                              magit-file-mode
                              magit-blob-mode
                              magit-blame-mode
                              )))
       "Emacs")
      ((derived-mode-p 'prog-mode)
       "Editing")
      ((derived-mode-p 'dired-mode)
       "Dired")
      ((memq major-mode '(helpful-mode
                          help-mode))
       "Help")
      ((memq major-mode '(org-mode
                          org-agenda-clockreport-mode
                          org-src-mode
                          org-agenda-mode
                          org-beamer-mode
                          org-indent-mode
                          org-bullets-mode
                          org-cdlatex-mode
                          org-agenda-log-mode
                          diary-mode))
       "OrgMode")
      (t
       (centaur-tabs-get-group-name (current-buffer))))))

  ; Keybinds for centaur tabs

  :bind
  ("C-<prior>" . centaur-tabs-backward)
  ("C-<next>" . centaur-tabs-forward)
  ("C-x tt" . centaur-tabs-switch-group))

(use-package smart-mode-line
  :init
  (setq sml/theme 'respectful)
  :config
  (sml/setup))

(use-package good-scroll
  :init
  (good-scroll-mode 1))

(use-package minions
  :delight " 𝛁"
  :config
  (minions-mode 1)
  (setq minions-mode-line-lighter "[+]")
  (setq minions-prominent-modes '(eyebrowse-mode)))

(use-package eyebrowse
  :init
  (eyebrowse-mode t)
  (eyebrowse-setup-opinionated-keys)
  :config
  (setq eyebrowse-new-workspace 'dashboard-open))


(use-package tree-sitter
  :ensure t
  :config
  ;; activate tree-sitter on any buffer containing code for which it has a parser available
  (global-tree-sitter-mode)
  ;; you can easily see the difference tree-sitter-hl-mode makes for python, ts or tsx
  ;; by switching on and off
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(use-package tree-sitter-langs
  :ensure t
  :after tree-sitter)

(use-package typescript-mode
  :after tree-sitter
  :config
  ;; we choose this instead of tsx-mode so that eglot can automatically figure out language for server
  ;; see https://github.com/joaotavora/eglot/issues/624 and https://github.com/joaotavora/eglot#handling-quirky-servers
  (define-derived-mode typescriptreact-mode typescript-mode
    "TypeScript TSX")

  ;; use our derived mode for tsx files
  (add-to-list 'auto-mode-alist '("\\.tsx?\\'" . typescriptreact-mode))
  ;; by default, typescript-mode is mapped to the treesitter typescript parser
  ;; use our derived mode to map both .tsx AND .ts -> typescriptreact-mode -> treesitter tsx
  (add-to-list 'tree-sitter-major-mode-language-alist '(typescriptreact-mode . tsx)))
